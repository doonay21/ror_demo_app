# README

Sample Ruby On Rails application for simple invoicing

Things you may want to cover:

* Ruby version: 2.4.4

* System dependencies: wkhtmltopdf binary

* Configuration: Manually create user or users.

* Database creation: rake db:migrate RAILS_ENV=production

* How to run the test suite: bundle exec rspec

To run sidekiq in production:
  bundle exec sidekiq -d -L log/sidekiq.log -C config/sidekiq.yml -e production

To run app in production:
  puma -C config/puma.rb -e production -d