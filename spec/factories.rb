FactoryBot.define do
  factory :invoice_element do
    item_name { Faker::Name.name }
    description { Faker::Name.name_with_middle }
    price { rand(0.01...10_000.0) }
    quantity { rand(1..100) }
  end

  factory :seller do
    company_name { Faker::Company.name }
    country { Faker::Address.country }
    post_code { Faker::Address.postcode }
    city { Faker::Address.city }
    address_1 { Faker::Address.street_name }
    address_2 { Faker::Address.street_address }
    vat_number { Faker::Company.polish_register_of_national_economy }
    prefix { Faker::Name.initials(2) }
  end

  factory :client do
    company_name { Faker::Company.name }
    country { Faker::Address.country }
    post_code { Faker::Address.postcode }
    city { Faker::Address.city }
    address_1 { Faker::Address.street_name }
    address_2 { Faker::Address.street_address }
    vat_number { Faker::Company.polish_register_of_national_economy }
  end
end
