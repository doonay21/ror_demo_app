require 'rails_helper'

RSpec.describe Invoice, type: :model do
  before(:each) do
    seller = create(:seller)
    client = create(:client)

    @invoice = Invoice.create(
      seller_id: seller.id,
      client_id: client.id
    )
  end

  it 'is valid with valid attributes' do
    expect(@invoice).to be_valid
  end

  it 'recalculates correctly with one element (2 * 100.0)' do
    invoice_element = create(:invoice_element,
                             invoice_id: @invoice.id,
                             price: 100.0, quantity: 2)

    net = 200.0
    tax = net * Invoice::VAT / 100.0
    gross = net + tax

    expect(invoice_element.invoice.net).to eq(net)
    expect(invoice_element.invoice.tax).to eq(tax)
    expect(invoice_element.invoice.gross).to eq(gross)
  end

  it 'recalculates correctly with two elements (1 * 12.9, 10 * 120.1)' do
    create(:invoice_element, invoice_id: @invoice.id, price: 12.9, quantity: 1)

    invoice_element = create(:invoice_element, invoice_id: @invoice.id,
                                               price: 120.1, quantity: 10)

    net = 1213.9
    tax = net * Invoice::VAT / 100.0
    gross = net + tax

    expect(invoice_element.invoice.net).to eq(net)
    expect(invoice_element.invoice.tax).to eq(tax)
    expect(invoice_element.invoice.gross).to eq(gross)
  end

  it 'recalculates correctly with element destroyed' do
    invoice_element = create(:invoice_element,
                             invoice_id: @invoice.id,
                             price: 100.0, quantity: 2)

    invoice_element.destroy

    expect(invoice_element.invoice.net).to eq(0.0)
    expect(invoice_element.invoice.tax).to eq(0.0)
    expect(invoice_element.invoice.gross).to eq(0.0)
  end

  describe 'Associations' do
    it 'belongs to seller' do
      association = described_class.reflect_on_association(:seller)
      expect(association.macro).to eq :belongs_to
    end

    it 'belongs to client' do
      association = described_class.reflect_on_association(:client)
      expect(association.macro).to eq :belongs_to
    end
  end
end
