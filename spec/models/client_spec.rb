require 'rails_helper'

describe Client do
  before(:each) do
    @client = create(:client)
  end

  it 'is valid with valid attributes' do
    expect(@client).to be_valid
  end

  it 'is not valid without a company_name' do
    @client.company_name = nil

    expect(@client).to_not be_valid
  end

  it 'is not valid without a country' do
    @client.country = nil

    expect(@client).to_not be_valid
  end

  it 'is not valid without a post_code' do
    @client.post_code = nil

    expect(@client).to_not be_valid
  end

  it 'is not valid without a city' do
    @client.city = nil

    expect(@client).to_not be_valid
  end

  it 'is not valid without an address_1' do
    @client.address_1 = nil

    expect(@client).to_not be_valid
  end

  it 'is valid without an address_2' do
    @client.address_2 = nil

    expect(@client).to be_valid
  end

  it 'is not valid without a vat_number' do
    @client.vat_number = nil

    expect(@client).to_not be_valid
  end

  it 'is not valid with not unique company name' do
    second_client = Client.create(
      company_name: @client.company_name,
      country: Faker::Address.country,
      post_code: Faker::Address.postcode,
      city: Faker::Address.city,
      address_1: Faker::Address.street_name,
      address_2: Faker::Address.street_address,
      vat_number: Faker::Company.polish_register_of_national_economy
    )

    expect(second_client).to_not be_valid
  end

  it 'is not valid with not unique vat number' do
    second_client = Client.create(
      company_name: Faker::Company.name,
      country: Faker::Address.country,
      post_code: Faker::Address.postcode,
      city: Faker::Address.city,
      address_1: Faker::Address.street_name,
      address_2: Faker::Address.street_address,
      vat_number: @client.vat_number
    )

    expect(second_client).to_not be_valid
  end

  describe 'Associations' do
    it 'has many invoices' do
      association = described_class.reflect_on_association(:invoices)
      expect(association.macro).to eq :has_many
    end
  end
end
