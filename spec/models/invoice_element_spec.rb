require 'rails_helper'

RSpec.describe InvoiceElement, type: :model do
  before(:each) do
    seller = create(:seller)
    client = create(:client)

    invoice = Invoice.create(
      seller_id: seller.id,
      client_id: client.id
    )

    @invoice_element = create(:invoice_element, invoice_id: invoice.id)
  end

  it 'is valid with valid attributes' do
    expect(@invoice_element).to be_valid
  end

  it 'is not valid without item_name' do
    @invoice_element.item_name = nil

    expect(@invoice_element).to_not be_valid
  end

  it 'is valid without description' do
    @invoice_element.description = nil

    expect(@invoice_element).to be_valid
  end

  it 'is not valid without price' do
    @invoice_element.price = nil

    expect(@invoice_element).to_not be_valid
  end

  it 'is not valid without quantity' do
    @invoice_element.quantity = nil

    expect(@invoice_element).to_not be_valid
  end

  describe 'Associations' do
    it 'belongs to invoice' do
      association = described_class.reflect_on_association(:invoice)
      expect(association.macro).to eq :belongs_to
    end
  end
end
