require 'rails_helper'

describe Seller do
  before(:each) do
    @seller = create(:seller)
  end

  it 'is valid with valid attributes' do
    expect(@seller).to be_valid
  end

  it 'is not valid without a company_name' do
    @seller.company_name = nil

    expect(@seller).to_not be_valid
  end

  it 'is not valid without a country' do
    @seller.country = nil

    expect(@seller).to_not be_valid
  end

  it 'is not valid without a post_code' do
    @seller.post_code = nil

    expect(@seller).to_not be_valid
  end

  it 'is not valid without a city' do
    @seller.city = nil

    expect(@seller).to_not be_valid
  end

  it 'is not valid without an address_1' do
    @seller.address_1 = nil

    expect(@seller).to_not be_valid
  end

  it 'is valid without an address_2' do
    @seller.address_2 = nil

    expect(@seller).to be_valid
  end

  it 'is not valid without a vat_number' do
    @seller.vat_number = nil

    expect(@seller).to_not be_valid
  end

  it 'is not valid without a prefix' do
    @seller.prefix = nil

    expect(@seller).to_not be_valid
  end

  it 'is not valid with not unique company name' do
    second_seller = Seller.create(
      company_name: @seller.company_name,
      country: Faker::Address.country,
      post_code: Faker::Address.postcode,
      city: Faker::Address.city,
      address_1: Faker::Address.street_name,
      address_2: Faker::Address.street_address,
      vat_number: Faker::Company.polish_register_of_national_economy,
      prefix: Faker::Name.initials(2)
    )

    expect(second_seller).to_not be_valid
  end

  it 'is not valid with not unique vat number' do
    second_seller = Seller.create(
      company_name: Faker::Company.name,
      country: Faker::Address.country,
      post_code: Faker::Address.postcode,
      city: Faker::Address.city,
      address_1: Faker::Address.street_name,
      address_2: Faker::Address.street_address,
      vat_number: @seller.vat_number,
      prefix: Faker::Name.initials(2)
    )

    expect(second_seller).to_not be_valid
  end

  it 'is not valid with not unique prefix' do
    second_seller = Seller.create(
      company_name: Faker::Company.name,
      country: Faker::Address.country,
      post_code: Faker::Address.postcode,
      city: Faker::Address.city,
      address_1: Faker::Address.street_name,
      address_2: Faker::Address.street_address,
      vat_number: Faker::Company.polish_register_of_national_economy,
      prefix: @seller.prefix
    )

    expect(second_seller).to_not be_valid
  end

  describe 'Associations' do
    it 'has many invoices' do
      association = described_class.reflect_on_association(:invoices)
      expect(association.macro).to eq :has_many
    end
  end
end
