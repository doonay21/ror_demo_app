class CreateInvoiceElements < ActiveRecord::Migration[5.2]
  def change
    create_table :invoice_elements do |t|
      t.integer :invoice_id, null: false
      t.string :item_name, null: false
      t.string :description, null: false
      t.decimal :price, precision: 8, scale: 2, null: false
      t.integer :quantity, null: false, default: 1

      t.timestamps
    end
  end
end
