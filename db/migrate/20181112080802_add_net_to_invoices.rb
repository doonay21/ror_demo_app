class AddNetToInvoices < ActiveRecord::Migration[5.2]
  def change
    add_column :invoices, :net, :decimal, precision: 12, scale: 2,
                                          after: :invoice_number, default: 0.0,
                                          null: false
    add_column :invoices, :tax, :decimal, precision: 12, scale: 2,
                                          after: :invoice_number, default: 0.0,
                                          null: false
    add_column :invoices, :gross, :decimal, precision: 12, scale: 2,
                                            after: :invoice_number,
                                            default: 0.0, null: false
  end
end
