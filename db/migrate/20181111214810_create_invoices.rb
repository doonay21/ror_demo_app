class CreateInvoices < ActiveRecord::Migration[5.2]
  def change
    create_table :invoices do |t|
      t.integer :seller_id, null: false
      t.integer :client_id, null: false
      t.integer :status, null: false, default: 0
      t.integer :year, null: false
      t.integer :month, null: false
      t.integer :iteration, null: false
      t.string :invoice_number, null: true

      t.timestamps
    end

    add_index :invoices, :invoice_number, unique: true
  end
end
