class CreateSellers < ActiveRecord::Migration[5.2]
  def change
    create_table :sellers do |t|
      t.string :company_name, null: false
      t.string :country, null: false
      t.string :post_code, null: false
      t.string :city, null: false
      t.string :address_1, null: false
      t.string :address_2
      t.string :vat_number, null: false
      t.string :prefix, null: false

      t.timestamps
    end

    add_index :sellers, :company_name, unique: true
    add_index :sellers, :vat_number, unique: true
    add_index :sellers, :prefix, unique: true
  end
end
