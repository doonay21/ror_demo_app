class CreateClients < ActiveRecord::Migration[5.2]
  def change
    create_table :clients do |t|
      t.string :company_name, null: false
      t.string :country, null: false
      t.string :post_code, null: false
      t.string :city, null: false
      t.string :address_1, null: false
      t.string :address_2
      t.string :vat_number, null: false

      t.timestamps
    end

    add_index :clients, :company_name, unique: true
    add_index :clients, :vat_number, unique: true
  end
end
