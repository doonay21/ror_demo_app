class SellersController < ApplicationController
  before_action do
    add_breadcrumb(t('sellers.name'), '/sellers')
  end

  def index
    all_sellers
  end

  def new
    @seller = Seller.new
  end

  def create
    @seller = Seller.new(seller_params)
    all_sellers

    render action: :new unless @seller.save
  end

  def edit
    @seller = Seller.find_by_id(params[:id])
  end

  def update
    @seller = Seller.find_by_id(params[:id])
    all_sellers

    render action: :edit unless @seller.update_attributes(seller_params)
  end

  def destroy
    seller = Seller.find_by_id(params[:id])
    seller.destroy

    all_sellers
  end

  def import
    import_file_path = ImportService.new(params[:file]).call

    SellersImportWorker.perform_async(current_user.id, import_file_path,
                                      columns_data,
                                      params[:skip_first_row] == '1')
  end

  private

  def all_sellers
    @sellers = SellerDecorator.decorate_collection(
      Seller.order(:company_name).page(params[:page])
    )
  end

  def seller_params
    params.require(:seller).permit(:company_name, :country, :post_code, :city,
                                   :address_1, :address_2, :vat_number, :prefix)
  end

  def columns_data
    params.permit(:column_1, :column_2, :column_3, :column_4, :column_5,
                  :column_6, :column_7, :column_8).to_h.values.map(&:to_i)
  end
end
