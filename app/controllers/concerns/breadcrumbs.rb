# Breadcrumbs concern contains logic for breadcrumbs handling
module Breadcrumbs
  extend ActiveSupport::Concern

  included do
    before_action do
      @breadcrumbs = []
    end
  end

  # Breadcrumb class holding name and path attributes
  class Breadcrumb
    attr_accessor :name, :path

    def initialize(name, path)
      self.name = name
      self.path = path
    end
  end

  # Adds single breadcrumb to collection
  def add_breadcrumb(name, path)
    @breadcrumbs << Breadcrumb.new(name, path)
  end
end
