class InvoiceElementsController < ApplicationController
  def new
    @invoice_element = InvoiceElement.new
  end

  def create
    @invoice_element = InvoiceElement.new(invoice_element_params)
    @invoice = InvoiceDecorator.decorate(@invoice_element.invoice)
    all_invoice_elements

    unless @invoice_element.save
      render action: :new
      return
    end

    @invoice_element = InvoiceElement.new
  end

  def destroy
    invoice_element = InvoiceElement.find_by_id(params[:id])
    @invoice = InvoiceDecorator.decorate(invoice_element.invoice)

    invoice_element.destroy

    @invoice_element = InvoiceElement.new
  end

  private

  def all_invoice_elements
    @invoice_elements = InvoiceElement.all
  end

  def invoice_element_params
    params.require(:invoice_element).permit(:invoice_id, :item_name,
                                            :description, :price, :quantity)
  end
end
