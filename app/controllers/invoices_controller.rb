class InvoicesController < ApplicationController
  before_action do
    add_breadcrumb(t('invoices.name'), '/invoices')
  end

  def index
    all_invoices
  end

  def show
    @invoice = InvoiceDecorator.decorate(Invoice.find_by_id(params[:id]))
    @invoice_element = InvoiceElement.new

    add_breadcrumb("#{t('invoices.invoice')} #{@invoice.invoice_number}",
                   invoice_path(@invoice))

    show_responders(@invoice)
  end

  def new
    @invoice = Invoice.new
  end

  def create
    @invoice = Invoice.new(invoice_params)
    all_invoices

    if @invoice.save
      redirect_to @invoice
    else
      render action: :new
    end
  end

  def destroy
    invoice = Invoice.find_by_id(params[:id])
    invoice.destroy

    all_invoices
  end

  def change_status
    invoice = Invoice.find_by_id(params[:id])
    invoice.status = invoice.status.zero? ? 1 : 0
    invoice.save

    all_invoices
  end

  private

  def all_invoices
    @invoices = Invoice.order(:seller_id, :year, :month, :iteration)
                       .page(params[:page])
  end

  def invoice_params
    params.require(:invoice).permit(:seller_id, :client_id)
  end

  def show_responders(invoice)
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "#{I18n.t('invoices.invoice')} #{invoice.invoice_number}",
               template: 'invoices/partials/invoice/_invoice.html.erb',
               layout: 'pdf.html.erb',
               dpi: 75
      end
    end
  end
end
