class ClientsController < ApplicationController
  before_action do
    add_breadcrumb(t('clients.name'), '/clients')
  end

  def index
    all_clients
  end

  def new
    @client = Client.new
  end

  def create
    @client = Client.new(client_params)
    all_clients

    render action: :new unless @client.save
  end

  def edit
    @client = Client.find_by_id(params[:id])
  end

  def update
    @client = Client.find_by_id(params[:id])
    all_clients

    render action: :edit unless @client.update_attributes(client_params)
  end

  def destroy
    client = Client.find_by_id(params[:id])
    client.destroy

    all_clients
  end

  def import
    import_file_path = ImportService.new(params[:file]).call

    ClientsImportWorker.perform_async(current_user.id, import_file_path,
                                      columns_data,
                                      params[:skip_first_row] == '1')
  end

  private

  def all_clients
    @clients = ClientDecorator.decorate_collection(
      Client.order(:company_name).page(params[:page])
    )
  end

  def client_params
    params.require(:client).permit(:company_name, :country, :post_code, :city,
                                   :address_1, :address_2, :vat_number)
  end

  def columns_data
    params.permit(:column_1, :column_2, :column_3, :column_4, :column_5,
                  :column_6, :column_7).to_h.values.map(&:to_i)
  end
end
