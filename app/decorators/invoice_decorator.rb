class InvoiceDecorator < ApplicationDecorator
  delegate_all
  decorates_association :invoice_elements
end
