class ClientDecorator < ApplicationDecorator
  delegate_all

  def full_address
    [object.address_1, object.address_2, object.city, object.post_code,
     object.country].reject(&:blank?).join(', ')
  end
end
