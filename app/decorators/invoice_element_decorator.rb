class InvoiceElementDecorator < ApplicationDecorator
  delegate_all

  def cost
    price * quantity
  end
end
