class ClientsImportWorker
  include Sidekiq::Worker
  require 'roo'

  def perform(user_id, file_path, columns, skip_first_row)
    import(user_id, file_path, columns, skip_first_row)
  end

  private

  def import(user_id, file_path, columns, skip_first_row)
    xlsx = Roo::Spreadsheet.open(file_path)
    sheet = xlsx.sheet(0)

    sheet.each_with_index do |row, index|
      next if skip_first_row && index.zero?

      handle_row(columns, row)
    end

    File.delete(file_path) if File.exist?(file_path)

    ActionCable.server.broadcast("notifications:#{user_id}",
                                 cmd: 'toastr',
                                 msg: I18n.t('clients.import_finished'))
  end

  def handle_row(columns, row)
    client = Client.new

    (0..6).each do |i|
      client.send("#{Client::COLUMNS[columns[i]][0]}=", row[i])
    end

    client.save
  end
end
