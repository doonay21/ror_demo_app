# ImportService contains methods used while importing data
class ImportService
  require 'fileutils'

  def initialize(source_file)
    @source_file = source_file
  end

  def call
    store_file_for_worker
  end

  private

  # Saves uploaded file in tmp/imports/clients
  def store_file_for_worker
    import_file_directory = Rails.root.join('tmp', 'imports')

    FileUtils.mkdir_p import_file_directory
    import_file_path = File.join(import_file_directory,
                                 "import_#{generate_random_name(16)}.xlsx")

    File.open(import_file_path, 'wb') do |file|
      file.write @source_file.read
    end

    import_file_path
  end

  # Generates random string with given length
  def generate_random_name(length)
    ('a'..'z').to_a.sample(length).join
  end
end
