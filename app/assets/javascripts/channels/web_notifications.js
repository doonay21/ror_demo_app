App.web_notifications = App.cable.subscriptions.create("NotificationsChannel", {
  connected: function() {
  },

  disconnected: function() {
  },

  received: function(data) {
    switch (data.cmd) {
      case 'toastr':
        toastr.success(data.msg)
        break;
    }
  }
});
