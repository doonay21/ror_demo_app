module SellersHelper
  def seller_columns_data_for_select
    Seller::COLUMNS.map do |column|
      [t("activerecord.attributes.seller.#{column[0]}"), column[1]]
    end
  end

  def sellers_ordered_by_company_name
    Seller.order(:company_name)
  end
end
