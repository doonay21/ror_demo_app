module ApplicationHelper
  include NavigationMenuHelper
  include BreadcrumbHelper

  # Generates danger alert containing resorce's errors
  def error_messages_for(recource)
    return unless recource.errors.any?

    ul = content_tag(:ul) do
      recource.errors.full_messages.each do |message|
        concat content_tag(:li, message)
      end
    end

    content_tag(:div, class: 'alert alert-danger', role: alert) do
      concat t('error_messages_title')
      concat ul
    end
  end
end
