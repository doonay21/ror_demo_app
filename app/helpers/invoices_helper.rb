module InvoicesHelper
  def invoice_status_icon(invoice)
    invoice.status.zero? ? 'badge-danger' : 'badge-success'
  end
end
