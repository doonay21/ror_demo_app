# Helper for navigation rendering
module NavigationMenuHelper
  # NavigationMenuItem class holding path and icon class attributes
  class NavigationMenuItem
    attr_accessor :path, :icon

    def initialize(path, icon)
      self.path = path
      self.icon = icon
    end
  end

  # Renders navigation menu
  def render_navigation_menu
    to_render = ''

    navigation_collection.each do |name, menu_item|
      to_render += render_menu_item(name, menu_item)
    end

    to_render
  end

  private

  # Returns hash containing navigation menu items to render
  def navigation_collection
    {
      'sellers' => NavigationMenuItem.new('/sellers', 'icon-wallet'),
      'clients' => NavigationMenuItem.new('/clients', 'icon-people'),
      'invoices' => NavigationMenuItem.new('/invoices', 'icon-doc')
    }
  end

  # Renders single menu item
  def render_menu_item(name, menu_item)
    tag_i = content_tag(:i, '', class:
      "nav-icon #{menu_item.icon}") + ' ' + t("main_menu.#{name}")

    tag_a_classes = ['nav-link']
    tag_a_classes << 'active' if controller_name == name

    tag_a = content_tag(:a, tag_i, class: tag_a_classes.join(' '),
                                   href: menu_item.path)

    content_tag(:li, tag_a, class: 'nav-item')
  end
end
