# Helper for breadcrumbs rendering
module BreadcrumbHelper
  # Renders breadcrumbs
  def render_breadcrumbs
    to_render = '<li class="breadcrumb-item">Home</li>'

    @breadcrumbs.each do |breadcrumb|
      to_render += render_breadcrumb(breadcrumb)
    end

    to_render
  end

  private

  # Renders single breadcrumb
  def render_breadcrumb(breadcrumb)
    if breadcrumb.path
      tag_a = content_tag(:a, breadcrumb.name, href: breadcrumb.path)
      content_tag(:li, tag_a, class: 'breadcrumb-item')
    else
      content_tag(:li, breadcrumb.name, class: 'breadcrumb-item')
    end
  end
end
