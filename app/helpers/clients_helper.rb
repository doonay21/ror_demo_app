module ClientsHelper
  def client_columns_data_for_select
    Client::COLUMNS.map do |column|
      [t("activerecord.attributes.client.#{column[0]}"), column[1]]
    end
  end

  def clients_ordered_by_company_name
    Client.order(:company_name)
  end
end
