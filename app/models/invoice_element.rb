class InvoiceElement < ApplicationRecord
  include InvoiceData

  belongs_to :invoice

  validates :item_name, length: { in: 3..100 }
  validates :description, length: { in: 10..255 }, allow_blank: true
  validates :price, presence: true,
                    numericality: { greater_than: 0, less_than: 10_000 },
                    format: { with: /\A\d{1,6}(\.\d{0,2})?\z/ }
  validates :quantity, length: { in: 1..100 }

  after_create :recalculate_invoice
  after_destroy :recalculate_invoice

  private

  def recalculate_invoice
    invoice_recalculate(invoice)
  end
end
