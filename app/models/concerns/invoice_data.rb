# InvoiceData concern contains logic for setting invoice number and date
module InvoiceData
  extend ActiveSupport::Concern

  def invoice_date_create(invoice)
    invoice.year = Time.current.year
    invoice.month = Time.current.month
  end

  def invoice_number_create(invoice)
    latest_invoice = Invoice.where(seller_id: invoice.seller_id)
                            .order(:year, :month, :iteration).last

    latest_invoice_number = latest_invoice.blank? ? 0 : latest_invoice.iteration
    invoice.iteration = latest_invoice_number + 1
    invoice.invoice_number = [invoice.seller.prefix, invoice.year,
                              invoice.month,
                              latest_invoice_number + 1].join('/')
  end

  def invoice_recalculate(invoice)
    net = invoice.invoice_elements.sum('price * quantity')

    invoice.net = net
    invoice.tax = net * Invoice::VAT / 100.0
    invoice.gross = invoice.net + invoice.tax
    invoice.save
  end
end
