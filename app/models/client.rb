class Client < ApplicationRecord
  has_many :invoices

  validates :company_name, length: { in: 3..255 }, uniqueness: true
  validates :country, length: { in: 4..100 }
  validates :post_code, length: { in: 3..20 }
  validates :city, length: { in: 2..100 }
  validates :address_1, length: { in: 2..200 }
  validates :address_2, length: { in: 2..200 }, allow_blank: true
  validates :vat_number, length: { in: 5..50 }, uniqueness: true

  COLUMNS = [
    ['company_name', 0],
    ['country', 1],
    ['post_code', 2],
    ['city', 3],
    ['address_1', 4],
    ['address_2', 5],
    ['vat_number', 6]
  ]
end
