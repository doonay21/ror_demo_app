class Invoice < ApplicationRecord
  include InvoiceData

  VAT = 20

  belongs_to :seller
  belongs_to :client
  has_many :invoice_elements, dependent: :destroy

  before_create :b_create

  private

  def b_create
    invoice_date_create(self)
    invoice_number_create(self)
  end
end
