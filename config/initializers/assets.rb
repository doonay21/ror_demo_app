# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )
Rails.application.config.assets.precompile << /\.(?:svg|eot|woff|ttf)\z/

Rails.application.config.assets.precompile <<
  %w[*.png *.jpg *.jpeg *.gif coreui-icons.min.css flag-icon.min.css \
     font-awesome.min.css simple-line-icons.css style.min.css style.min.css \
     pace.min.css popper.min.js bootstrap.min.js pace.min.js \
     perfect-scrollbar.min.js coreui.min.js jquery.min.js toastr.min.js \
     toastr.min.css]
