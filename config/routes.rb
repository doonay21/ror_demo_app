Rails.application.routes.draw do
  devise_for :users
  root to: 'sellers#index'

  mount ActionCable.server => '/cable'

  resources :sellers do
    collection do
      post :import
    end
  end

  resources :clients do
    collection do
      post :import
    end
  end

  resources :invoices do
    collection do
      get :change_status
    end
  end

  resources :invoice_elements
end
